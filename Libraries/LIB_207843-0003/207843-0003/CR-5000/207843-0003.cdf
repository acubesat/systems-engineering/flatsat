(part "207843-0003"
    (packageRef "2078430003")
    (interface
        (port "1" (symbPinId 1) (portName "1") (portType INOUT))
        (port "2" (symbPinId 2) (portName "2") (portType INOUT))
        (port "3" (symbPinId 3) (portName "3") (portType INOUT))
    )
    (partClass UNDEF)
    (useInSchema Y)
    (useInLayout Y)
    (inPartsList Y)
    (partType NORMAL)
    (placeRestriction FREE)
    (property "compKind" "181")
    (property "Mouser_Part_Number" "538-207843-0003")
    (property "Mouser_Price/Stock" "https://www.mouser.co.uk/ProductDetail/Molex/207843-0003?qs=XeJtXLiO41TegXAHRiYnCg%3D%3D")
    (property "Manufacturer_Name" "Molex")
    (property "Manufacturer_Part_Number" "207843-0003")
    (property "Description" "Headers & Wire Housings HEADER ASSEMBLY FOR L1NK 250 1X3 Ckt")
    (property "Datasheet_Link" "https://www.molex.com/pdm_docs/sd/2078430003_sd.pdf")
    (property "symbolName1" "207843-0003")
)

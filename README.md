# FlatSat



## Introduction

The initial development and
testing stages of the satellite are to take place on
a Flatsat model, where the subsystem models will
be gradually integrated.

This repository contains the design files of the FlatSat modules as well as supporting documents.

## High-level overview

![FlatSatOverview](/media/FlatSatOverview.png)


## Disclaimer

The AcubeSAT project is carried out with the support of the Education Office of
the [European Space Agency](https://esa.int), under the educational
[Fly Your Satellite!](https://www.esa.int/Education/CubeSats_-_Fly_Your_Satellite/)
programme.

This repository has been authored by university students participating in the
AcubeSAT project. The views expressed herein by the authors can in no way be
taken to reflect the official opinion, or endorsement, of the European Space
Agency.
